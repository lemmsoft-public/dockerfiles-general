#!/bin/bash
echo 'Preparing PG_DATA...'
if [ -f $PGDATA/initial_setup_done ]
then
	echo 'PG_DATA already prepared.'
else
	chown -R postgres:postgres "$PGDATA" && chmod 750 "$PGDATA"
	rm -rf $PGDATA
	su -c '/usr/lib/postgresql/9.6/bin/initdb -D "$PGDATA" --encoding=UTF8 --no-locale' postgres
	sed -i -e "s/# listen_addresses = 'localhost'/listen_addresses = '*'/g" $PGDATA/postgresql.conf
	echo 'host all  all    0.0.0.0/0  md5' >> $PGDATA/pg_hba.conf
	echo '1' >> $PGDATA/initial_setup_done
	echo 'PG_DATA prepared successfully.'
fi