#!/bin/bash
# This script is used to install vscode inside the dev containers. It downloads the package and
# installs the editor itself, as well as the provided list of extensions. The main point of this is
# to allow work to be done from within the container using the vscode Remote extensions pack, thus
# adding support for debugging as if one were working directly in the host system.

yes | code-server \
  --install-extension eamodio.gitlens \
  --install-extension ms-python.vscode-pylance \
  --install-extension dbaeumer.vscode-eslint \
  --install-extension visualstudioexptteam.vscodeintellicode \
  --install-extension visualstudioexptteam.intellicode-api-usage-examples \
  --install-extension esbenp.prettier-vscode \
  --install-extension ms-python.python \
  --install-extension github.copilot \
  --install-extension github.copilot-chat
# cp -r $HOME/.local/share/code-server $HOME/.vscode-server
mkdir -p $HOME/.vscode-server/data
cp -r $HOME/.local/share/code-server/* $HOME/.vscode-server/data/
