#!/bin/bash
ROOT_USER_IS_SET_UP="0"
if [ -f /root_user_is_set_up ]; then
  ROOT_USER_IS_SET_UP=$(cat /root_user_is_set_up)
fi
if [ $ROOT_USER_IS_SET_UP = "0" ]; then
  echo "MySQL root user not set up. Setting it up...\n"
  sh /kill_mysql_process.sh && \
    /usr/bin/mysqld_safe --user=mysql --daemonize --skip-grant-tables --skip-networking
  # sleep 3
  rm -f /root_user_setup_sql_completed.txt
  mysql -u root < "/root_user_setup.sql" && \
    echo "1" > "/root_user_setup_sql_completed.txt"
  if [ ! -f /root_user_setup_sql_completed.txt ] || [ "$(cat /root_user_setup_sql_completed.txt)" != "1" ]; then
    echo "Failed to execute script root_user_setup.sql."
    exit 1
  fi
  echo "Script root_user_setup.sql executed successfully.\n"
  sh /kill_mysql_process.sh
  /usr/sbin/mysqld --user=mysql --daemonize --log-error
  rm -f /root_user_setup_2_sql_completed.txt
  mysql -u root --connect-expired-password < "/root_user_setup_2.sql" && \
    echo "1" > "/root_user_setup_2_sql_completed.txt"
  if [ ! -f /root_user_setup_2_sql_completed.txt ] || [ "$(cat /root_user_setup_2_sql_completed.txt)" != "1" ]; then
    echo "Failed to execute script root_user_setup_2.sql."
    exit 1
  fi
  echo "Script root_user_setup_2.sql executed successfully.\n"
  echo "MySQL root user set up successfully. Server started.\n"
  rm -f /root_user_is_set_up && echo "1" > /root_user_is_set_up
else
  echo "MySQL root user already set up. Starting server.\n"
  /usr/sbin/mysqld --user=mysql --daemonize --log-error
fi
