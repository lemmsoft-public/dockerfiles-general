#!/bin/bash
# This script is used to set up render-access users in containers where the
# commands need to be executed as a non-root user and have access to the GPU.
# It expects the user_setup script to have been ran first.

# Check whether the group already exists (by name and id)
SKIP_GROUP="0"
if [ $(grep -q -E "^$RENDER_GROUP_NAME:" /etc/group) ]; then
  echo "Group $RENDER_GROUP_NAME already exists. Skipping."
  if [ $(grep -q -E "\\*:$RENDER_GROUP_ID:" /etc/group) ]; then
    echo "Error: Group $RENDER_GROUP_ID does not exist. Please check your env vars and try again."
    exit 1
  fi
  SKIP_GROUP="1"
fi

if [ $SKIP_GROUP = "0" ]; then
  groupadd -g $RENDER_GROUP_ID $RENDER_GROUP_NAME
fi
usermod -aG $RENDER_GROUP_NAME $LOCAL_USER_NAME
