#!/bin/bash
# This script is used to fix the issue where webpack forces node to throw a max call stack size exceeded
# exception when building large projects (the default value is 2GB of RAM). It renames the nodejs
# executable and adds a shell script in its place. Said script calls the renamed nodejs executable, but
# adds a flag for increasing the max call stack size to 10GB.
# Also, it sets the --dns-result-order=ipv4first flag, so that we have forced ipv4 DNS resolition at least in
# some circumstances.
mv ~/.nvm/versions/node/v$NODE_VERSION/bin/node ~/.nvm/versions/node/v$NODE_VERSION/bin/_node
printf "#!/bin/bash\n~/.nvm/versions/node/v$NODE_VERSION/bin/_node --max-old-space-size=10240 \"\$@\"\n" > ~/.nvm/versions/node/v$NODE_VERSION/bin/node
chmod +x ~/.nvm/versions/node/v$NODE_VERSION/bin/node
