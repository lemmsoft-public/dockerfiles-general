#!/bin/bash
# initialize mysql manually, but only if it's the first run for this container's volume; this script does the following:
# 1. checks if we've already ran the script; exits if he have, otherwise:
# 2. sets the "is first run" flag to 0 for future use
# 3. grants log-in rights to the mysql user
# 4. adds port 3306 to the mysql config file
# 5. changes the datadir in the mysql config file to the dedicated one mounted by the docker service
# 6. empties the mysql data dir, because the install puts some data in but it's not usable and produces initialization errors
# 7. initializes the mysql server so it can be launched as a daemon on container start
IS_FIRST_RUN="1"
if [ -f /is_first_run ]; then
  IS_FIRST_RUN="$(cat /is_first_run)"
fi
if [ $IS_FIRST_RUN = "1" ]; then
  echo "MySQL not initialized, initializing...\n"
  usermod -s /bin/bash mysql
  sed -i -E "s~\port\s+= 3306~port = $(cat /mysql_port)~g" /etc/mysql/mysql.conf.d/mysqld.cnf
  sed -i -E "s~\#\s+?port~port~g" /etc/mysql/mysql.conf.d/mysqld.cnf
  sed -i -E "s~\datadir\s+= /var/lib/mysql~datadir = /mysql_data~g" /etc/mysql/mysql.conf.d/mysqld.cnf
  sed -i -E "s~\#\s+?datadir~datadir~g" /etc/mysql/mysql.conf.d/mysqld.cnf
  sed -i -E "s~bind-address\s+= 127.0.0.1~bind-address = 0.0.0.0~g" /etc/mysql/mysql.conf.d/mysqld.cnf
  sed -i -E "s~\#\s+?bind-address~bind-address~g" /etc/mysql/mysql.conf.d/mysqld.cnf
  chown -R mysql:mysql /mysql_data
  sh /kill_mysql_process.sh
  rm -f /mysql_init_result.txt
  /usr/sbin/mysqld --user=mysql --initialize && echo "1" > /mysql_init_result.txt
  if [ ! -f /mysql_init_result.txt ] || [ "$(cat /mysql_init_result.txt)" != "1" ]; then
    echo "Failed to initialize MySQL."
    exit 1
  fi
  echo "MySQL initialized successfully.\n"
  rm -f /is_first_run && echo "0" > /is_first_run
else
  echo "MySQL already initialized.\n"
fi
