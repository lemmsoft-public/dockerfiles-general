mysql_process=$(top -b -n 1 | grep mysql)
mysql_process=$(echo "${mysql_process%%mysql*}")
if [ ! -z $mysql_process ]; then
  kill $mysql_process
  while [ ! -z "$(top -b -n 1 | grep mysql)" ]
  do
    echo "Waiting for mysql process to stop..."
    sleep 1
  done
fi
