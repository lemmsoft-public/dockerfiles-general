#!/bin/bash
# This script is used to set up users in containers where the commands need to be executed as a
# non-root user. It sets up the container non-root user to match the provided local user.

# Check whether the group already exists (by name and id)
SKIP_GROUP="0"
if [ $(grep -q -E "^$LOCAL_USER_GROUP_NAME:" /etc/group) ]; then
  echo "Group $LOCAL_USER_GROUP_NAME already exists. Skipping."
  if [ $(grep -q -E "\\*:$LOCAL_USER_GROUP_ID:" /etc/group) ]; then
    echo "Error: Group $LOCAL_USER_GROUP_ID does not exist. Please check your env vars and try again."
    exit 1
  fi
  SKIP_GROUP="1"
fi

# Check whether the user already exists (by name and id)
SKIP_USER="0"
if [ $(grep -q -E "^$LOCAL_USER_GROUP_NAME:" /etc/group) ]; then
  echo "Group $LOCAL_USER_GROUP_NAME already exists. Skipping."
  if [ $(grep -q -E "\\*:$LOCAL_USER_GROUP_ID:" /etc/group) ]; then
    echo "Error: Group $LOCAL_USER_GROUP_ID does not exist. Please check your env vars and try again."
    exit 1
  fi
  SKIP_USER="1"
fi

if [ $SKIP_GROUP = "0" ]; then
  groupadd -g $LOCAL_USER_GROUP_ID $LOCAL_USER_GROUP_NAME
fi
if [ $SKIP_USER = "0" ]; then
  useradd -m -l -u $LOCAL_USER_ID -g $LOCAL_USER_GROUP_NAME $LOCAL_USER_NAME -s /bin/bash -p '*'
  mkdir -p /home/$LOCAL_USER_NAME
  chown -R $LOCAL_USER_NAME:$LOCAL_USER_GROUP_NAME /home/$LOCAL_USER_NAME
  usermod -aG sudo,video,audio $LOCAL_USER_NAME
fi
printf "$LOCAL_USER_PASSWORD\n$LOCAL_USER_PASSWORD" passwd $LOCAL_USER_NAME
